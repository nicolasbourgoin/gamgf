# GAMGF App

GAMGF App is a simple App to refund money with friends

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

`
ruby
`

`
rails 5.1.3
`

### Installing

A step by step series of tasks that to do to get a development env running

1.

Git Colne
- SSH : `git@gitlab.com:nicolasbourgoin/gamgf.git`
- HTTPS `https://gitlab.com/nicolasbourgoin/gamgf.git`

2.

```
cd gamgf
bundle install --without production
bundle exec rake db:migrate
bundle exec rake db:test:prepare
bundle exec rspec spec/
```


## Running the tests

Run

```
rails test
```


## Deployment

Deploy App on Heroku

Link : [GAMGF App](https://gamgf.herokuapp.com/) on Heroku


## Built With



## Contributing



## Versioning



## Authors



## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
*
