Rails.application.routes.draw do
  get 'users/new'

  get 'sessions/new'

  get 'expenses/new'
  post 'expenses/new'

  get 'welcome/index'
  #root 'welcome#index'
  resources :expenses



  root 'static_pages#home'
  get  '/help',    to: 'static_pages#help'
  get  '/about',   to: 'static_pages#about'
  get  '/contact', to: 'static_pages#contact'
  get  '/signup',  to: 'users#new'
  post '/signup',  to: 'users#create'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'

  resources :users
  get	'/users/index'
  #root 'users#index'

#GET	/users/1	show	page to show user with id 1
	get '/users/new'
	#post '/users'	, to: 'users#destroy'#create	create a new user
  get	'/users/1/edit'	, to: 'users#edit' #edit	page to edit user with id 1
  #patch	'/users/1'	#update	update user with id 1
  post	'/users/1/destroy'	, to: 'users#destroy' #destroy	delete user with id 1



  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
